window.addEventListener('load', function () {
	//Recuperation des jours à afficher, et des boutons
    var $jours = $('.jour');
    var $puces = $('.entypo-record');

    //Initialisation des animations
    function init() {
        setTimeout(function() {
            $('body').addClass('isok');
            $jours.hide();
            $('.wrapper').fadeIn('slow', function() {
                $jours.first().fadeIn('slow');
                $puces.removeClass('active').first().addClass('active');
            });
        }, 2000);
    }
    

    //Clic sur un des boutons en bas de page (selection du jour)
    $puces.click(function() {
        var $this = $(this);
        var cible = $this.attr('data-cible');
        $jours.hide();
        $($jours.get(cible)).fadeIn()
        $puces.removeClass('active');
        $this.addClass('active');
    });

    init();
})
